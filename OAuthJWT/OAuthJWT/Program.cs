﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.IdentityModel.Protocols;
using System.IdentityModel.Tokens;

namespace OAuthJWT
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string stsDiscoveryEndpoint = "https://login.microsoftonline.com/common/v2.0/.well-known/openid-configuration";
            ConfigurationManager<OpenIdConnectConfiguration> configManager = new ConfigurationManager<OpenIdConnectConfiguration>(stsDiscoveryEndpoint);
            OpenIdConnectConfiguration config = configManager.GetConfigurationAsync().Result;
            string clientId = "0fa74f3b-11d1-4af0-9858-43fc1605d295";
            
            // get id_token/refresh_token using authentication code programatically.
            var request = (HttpWebRequest)WebRequest.Create(config.TokenEndpoint);
            NameValueCollection queryString = HttpUtility.ParseQueryString(String.Empty);
            queryString.Add("grant_type", "authorization_code");
            queryString.Add("client_id", clientId);
            queryString.Add("code", "M1f65a2c2-3814-9dfc-0648-cd306224e126");
            queryString.Add("redirect_uri", "http://localhost:18001");
            queryString.Add("client_secret", "OpxcqPVMubmvwWvtYhYtXkS");
            string postData = queryString.ToString();
            var data = Encoding.UTF8.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
                        var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            

            // validate return id_token(jwt format)
            /*
            string jwtString = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlliUkFRUlljRV9tb3RXVkpLSHJ3TEJiZF85cyJ9.eyJhdWQiOiIwZmE3NGYzYi0xMWQxLTRhZjAtOTg1OC00M2ZjMTYwNWQyOTUiLCJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vNzJmOTg4YmYtODZmMS00MWFmLTkxYWItMmQ3Y2QwMTFkYjQ3L3YyLjAiLCJpYXQiOjE0NzI2MjAyNzQsIm5iZiI6MTQ3MjYyMDI3NCwiZXhwIjoxNDcyNjI0MTc0LCJuYW1lIjoiTFUgR0RJIFNlcnZpY2UiLCJvaWQiOiJjMzhkYmMzMS02OTRlLTRkYzktYTY5Mi0wNWM2YzU2MzU3MDUiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJsdWdkaXNydkBtaWNyb3NvZnQuY29tIiwic3ViIjoiUVNrck1nU2Z5SzVqaEFFQ1VkMUxmN09qRUNTYjlMLU1iNkYwZlh0VnI4WSIsInRpZCI6IjcyZjk4OGJmLTg2ZjEtNDFhZi05MWFiLTJkN2NkMDExZGI0NyIsInZlciI6IjIuMCJ9.M8etKeqtGodP6UF-7iq3_2RuisfT31Brgb7MsFdY-z0aj-6VTzD3DY71xhVUfpFJ1UShUlsMvYDYu2TCR5-P_KLW6BKYmaZrWztrcPqLC90n_qrjt5R05JF9LB9Rmzb2tydoi9W9zVWwGJ0cYi6Rw8yAoopQmtUk29LgvEyDw9cbYHHhadnxT76NiBCp8yH4QuPUVlNHg2UekNXKxlLvpb2gGcx9hETOiK2ZiogOeVms_GPdSmVM5M23Tl0C21xZuBy8dhmf5MBgl9INvi_dzZusIGT1elUS2fHeOiMmSeACZ6JyMrMOffPbyk9AtK3F92bPjCOV3aC3vNVvx0xzPw";
            var validationParameters = new TokenValidationParameters()
            {
                ValidAudience = clientId,
                ValidateIssuer = false,
                ValidateLifetime = true,
                IssuerSigningTokens = config.SigningTokens,
            };
            JwtSecurityTokenHandler tokendHandler = new JwtSecurityTokenHandler();
            SecurityToken st;
            var result = tokendHandler.ValidateToken(jwtString, validationParameters, out st);
            JwtSecurityToken jwt = st as JwtSecurityToken;
            var email = jwt.Payload["preferred_username"];
            var name = jwt.Payload["name"];*/
        }
    }
}

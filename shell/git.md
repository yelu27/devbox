# Git

## file operations

**rename a file**

    git mv <src file> <dst file>

**revert a file**

	# revert specific file to the last version before current HEAD. head can be replaced with sha of any commit.
	git checkout head~1 file/to/restore

## local branch operations

**list local branches**

    # should show all the local branches of your repo. The starred branch is your current working branch.
    git branch

**switch branch**

    # switch to 'develop' as working branch
    git checkout develop

## remote resposity operations

**print remote address**

    git remote -v

**add a remote repository**

    git remote add [shortname] [url]

**push local branch to remote repository**

    git push [remote repository name] [local branch name]:[remote branchy name]

if [local branch name] is the same as [remote branchy name], short command:

    git push [remote repository name] [local branch name]
    git push origin local_branch

**clone only one file**

git archive --remote=https://github.com/yelu/CodeSnack.git HEAD:CodeSnack .vimrc | tar -x

**push to another remote repository**

    git remote add origin https://yelu27@bitbucket.org/yelu27/lccrf.git
    git push -u origin --all # pushes up the repo and its refs for the first time
    git push origin --tags # pushes up any tags
    
**pull remote branch to current branch**

pull the master branch from the remote called origin into your **current** branch. It only affects your current branch, not your local master branch.

    git pull origin master
    
the same as
    
    git fetch; git merge origin/master

## git submodule

**Add submodule, and meanwhile, tracking remote master branch**

    git submodule add -b master [URL to Git repo]

**Update submodules, and if they're not initiated yet, will initiate them, recursively**

    git submodule update --init --remote --recursive

**remove a submodule**

    git submodule deinit -f CarinaRCTools
    git rm CarinaRCTools
    rm -rf .git/modules/CarinaRCTools

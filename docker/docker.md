## docker

**install**

    sudo wget -qO- https://get.docker.com/ | sh

**build an image**
    
    sudo docker build -t queryindex_s2s:1rc0 .

**run**

    # run an image in a container
    sudo docker run queryindex_s2s:1rc3 /model/run.sh http
    # run an image in interactive mode
    sudo docker run -it --entrypoint=/bin/bash queryindex_s2s:1rc1

**save**
    
    #  saves a non-running container image to a tar file
    sudo docker save -o queryindex_s2s.tar queryindex_s2s:1rc1
    # saves a containerís running or paused instance to a tar file
    sudo docker pull ubuntu
    sudo docker run -t -i -d ubuntu /bin/bash
    sudo docker export bd227533eeb5 > docker_exported.tar

**check running docker containers**

    sudo docker ps

**stop docker containers**

    sudo docker stop 28f4c09d0628
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using System.IO;
using System.Diagnostics;

namespace VSOSync
{
    public class Repo
    {
        public string Name;
        public string Path;
        public LibGit2Sharp.Repository GitRepo = null;
        public List<Repo> SubModules = new List<Repo>();
    }

    public class Syncer
    {
        public Syncer(string rootDir, string token)
        {
            RootDir = rootDir;
            Token = token;

            // if rootDir is not a git repo. assume folders inside may be git repos.
            if (Repository.IsValid(rootDir))
            {
                repo = GetRepo(RootDir, Token);
            }
            else
            {
                repo = new Repo()
                {
                    GitRepo = null
                };
                DirectoryInfo di = new DirectoryInfo(rootDir);
                foreach (var d in di.EnumerateDirectories())
                {
                    if (Repository.IsValid(d.FullName))
                    {
                        repo.SubModules.Add(GetRepo(d.FullName, Token));
                    }
                }
            }
        }

        private string Token;
        public string RootDir { get; private set; }

        private Repo repo;

        public void Print()
        {
            Print(repo);
        }

        public static void Print(Repo root, string prefix = "")
        {
            if (root == null)
            {
                Console.WriteLine(prefix + "+- <null>");
                return;
            }

            string line = string.Format("{0}+- {1} ({2})", prefix, root.Name, root.Path);
            Console.WriteLine(line);
            foreach (var subModule in root.SubModules)
            {
                Print(subModule, "|  " + prefix);
            }
        }


        public void Sync()
        {
            Sync(this.repo);
        }

        public static void Sync(Repo repo)
        {
            foreach (var r in repo.SubModules)
            {
                Sync(r);
            }
            if (string.IsNullOrWhiteSpace(repo.Path))
            {
                return;
            }
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("");
            string msg = string.Format("Sync repository {0} ({1})", repo.Name, repo.Path);
            Console.WriteLine(msg);
            Console.ResetColor();
            string comment = string.Format("update repository {0} on {1}", repo.Name, Environment.MachineName);
            GitSync(repo.Path, comment);
        }

        public static Repo GetRepo(string dir, string token)
        {
            Repo ret = new Repo();
            // detect if it is a valid git repo.
            if (!Repository.IsValid(dir))
            {
                throw new Exception(string.Format("Invalid git repo in \"{0}\"", dir));
            }
            ret.Name = Path.GetFileName(dir.TrimEnd('/').TrimEnd('\\'));
            ret.Path = dir;
            ret.GitRepo = new Repository(dir);
            ret.GitRepo.Checkout("master");
            // get all submodules.
            var subModules = ret.GitRepo.Submodules;         
            foreach (var subModule in subModules)
            {          
                string path = Path.Combine(dir, subModule.Path);
                // clone submodule if it has not be cloned.
                if (!Repository.IsValid(path))
                {
                    SubmoduleUpdate(dir, subModule.Name, subModule.Path);
                }
                ret.SubModules.Add(GetRepo(path, token));               
            }
            return ret;
        }

        public static void SubmoduleUpdate(string dir, string moduleName, string modulePath)
        {
            RunGitCmd(dir, string.Format("git submodule update --remote --init {0}", moduleName));
            RunGitCmd(Path.Combine(dir, modulePath), string.Format("git checkout master"));
        }

        public static void GitSync(string dir, string comment)
        {          
            RunGitCmd(dir, string.Format("git checkout master && git add -u && git diff-index --quiet HEAD || git commit -m \"{0}\"", comment));
            RunGitCmd(dir, string.Format("(git pull origin master && git add -u && git diff-index --quiet HEAD || git commit -m \"auto merge\") && git push origin master"));
            Console.ForegroundColor = ConsoleColor.DarkRed;
            RunGitCmd(dir, string.Format("git ls-files --exclude-standard --others --directory"));
            Console.ResetColor();
        }

        public static void RunGitCmd(string workingDir, string cmd, string indent = "    ")
        {
            Process process = new Process();
            process.StartInfo.FileName = "cmd.exe";
            process.StartInfo.WorkingDirectory = workingDir;
            process.StartInfo.Arguments = string.Format("/c {0}", cmd);
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = true;

            process.ErrorDataReceived += (s, e) =>
            {
                if (e.Data != null)
                {
                    Console.WriteLine(indent + e.Data);
                }
            };
            process.OutputDataReceived += (s, e) =>
            {
                if (e.Data != null)
                {
                    Console.WriteLine(indent + e.Data);
                }
            };

            Console.WriteLine(string.Format("{0}", process.StartInfo.Arguments.Substring(3)));
            process.Start();
            process.BeginErrorReadLine();
            process.BeginOutputReadLine();
            process.WaitForExit();         
            if (process.ExitCode != 0)
            {
                throw new Exception(string.Format("{0} exit with error code {1}", process.StartInfo.FileName, process.ExitCode));
            }
            process.Close();
        }



    }
}

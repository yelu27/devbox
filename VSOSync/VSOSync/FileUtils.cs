﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.IO.Compression;

namespace CarinaRC.Library
{    
    public class LocalOrSharedFile 
    {
        public LocalOrSharedFile(string path)
        {
            FilePath = NormalizePath(path);
        }

        public string FilePath { get; protected set; }

        protected long? Length;
        public long? GetLength()
        {
            if (Length == null)
            {
                var info = new System.IO.FileInfo(FilePath);
                Length = info.Length;
            }
            return Length;
        }

        protected DateTime? CreateTime;
        public DateTime? GetCreateTime()
        {
            if (CreateTime == null)
            {
                var info = new System.IO.FileInfo(FilePath);
                CreateTime = info.LastWriteTimeUtc;
            }
            return CreateTime;
        }

        protected string Md5;
        public string GetMd5()
        {
            if (string.IsNullOrWhiteSpace(Md5))
            {
                Md5 = FileMD5(FilePath);
            }
            return Md5;
        }

        public static string NormalizePath(string path)
        {
            var dstPath = Path.GetFullPath(path.Trim());
            return dstPath;
        }

        public static IEnumerable<string> EnumerateFiles(string root, string relativePath = "")
        {
            string dir = Path.Combine(root, relativePath);
            if (!Directory.Exists(dir))
            {
                throw new Exception(string.Format("\"{0}\" is not a directory.", dir));
            }
            
            var directoryInfo = new DirectoryInfo(dir);
            foreach (var item in directoryInfo.GetFiles())
            {
                if (item.Name.StartsWith(".git")) { continue; }
                yield return Path.Combine(relativePath, item.Name);
            }
            foreach (var item in directoryInfo.GetDirectories())
            {
                // skip .git folder
                if (item.Name.ToLower() == ".git") { continue; }
                string newRelativePath = Path.Combine(relativePath, item.Name);
                foreach (var file in EnumerateFiles(root, newRelativePath))
                {
                    yield return file;
                }
            }
        }

        public static void RemoveEmptyDirRecursive(string root)
        {
            if (!Directory.Exists(root)) { return; }
            var directoryInfo = new DirectoryInfo(root);
            foreach (var item in directoryInfo.GetDirectories())
            {
                if (item.Name.ToLower() == ".git") { continue; }
                RemoveEmptyDirRecursive(item.FullName);
            }
            directoryInfo = new DirectoryInfo(root);
            if (directoryInfo.GetDirectories().Count() == 0 &&
                directoryInfo.GetFiles().Count() == 0)
            {
                Directory.Delete(root);
            }
        }

        public static void DeleteDirRecursive(string root)
        {
            string[] files = Directory.GetFiles(root);
            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }
            string[] dirs = Directory.GetDirectories(root);
            foreach (string dir in dirs)
            {
                DeleteDirRecursive(dir);
            }
            Directory.Delete(root, false);
        }

    }

}

﻿using CommandLine;
using CommandLine.Text;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSOSync
{
    class Program
    {
        class Options
        {
            [Option('d', "Directory", Required = true,
                    HelpText = "Directory of the git repo")]
            public string Directory { get; set; }

            [Option('p', "Print", Required = false, DefaultValue = true,
                    HelpText = "Print structure of repos or not.")]
            public bool IsPrint { get; set; }

            [Option('s', "Sync", Required = false, DefaultValue = false,
                    HelpText = "Sync repos or not")]
            public bool IsSync { get; set; }

            [ParserState]
            public IParserState LastParserState { get; set; }

            [HelpOption]
            public string GetUsage()
            {
                return HelpText.AutoBuild(this,
                  (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
            }
        }

        static void Main(string[] args)
        {
            var options = new Options();
            if (!CommandLine.Parser.Default.ParseArguments(args, options))
            {
                return;
            }

            if (!Directory.Exists(options.Directory))
            {
                throw new Exception("Directory not found");
            }

            string token = ConfigurationManager.AppSettings["VSOToken"];
            Syncer vs = new Syncer(options.Directory.Trim(), token);
            if (options.IsPrint)
            {
                Console.WriteLine("Repositories:");
                vs.Print();
            }

            if (options.IsSync)
            {
                vs.Sync();
                Console.WriteLine("Sync done successfully!");
            }
        }
    }
}

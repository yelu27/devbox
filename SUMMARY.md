# Summary

* Shell
  * [bash](shell/bash.md)
  * [python](shell/python.md)
  * [sed & awk](shell/sed_awk.md)
  * [git](shell/git.md)
  * [ubuntu env setup](shell/ubuntu_initenv.md)
